using System;
using Newtonsoft.Json;

namespace ChatServer.API.Dtos
{
    public class UserDto
    {
        [JsonProperty(PropertyName = "user_id")]
        public Guid UserId { get; set; }
        [JsonProperty(PropertyName = "user_name")]
        public string UserName { get; set; }
    }
}