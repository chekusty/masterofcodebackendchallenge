using Newtonsoft.Json;

namespace ChatServer.API.Dtos
{
    public class RoomCreateDto
    {
        [JsonProperty(PropertyName = "user_id")]
        public string UserId{get;set;}

        [JsonProperty(PropertyName = "name")]
        public string RoomName{get;set;}
    }
}