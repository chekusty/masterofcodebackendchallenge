using Newtonsoft.Json;

namespace ChatServer.API.Dtos
{
    public class UserForJoiningTheRoom
    {
        [JsonProperty(PropertyName = "user_id")]
        public string UserId { get; set; }
    }
}