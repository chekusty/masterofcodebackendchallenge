using System;
using Newtonsoft.Json;

namespace ChatServer.API.Dtos
{
    public class UserLoginResultDto : UserDto
    {
        [JsonProperty(PropertyName = "credentials")]
        public string Credentials { get; set; }
    }
}