using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ChatServer.API.Dtos
{
    public class UserForLoginDto
    {
        [JsonProperty(PropertyName = "user_name")]
        public string Username{get;set;}
        
        [StringLength(16, MinimumLength = 4, ErrorMessage = "Weak password")]
        public string Password{get;set;}
    }
}