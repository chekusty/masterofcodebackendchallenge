using Newtonsoft.Json;

namespace ChatServer.API.Dtos
{
    public class MessageResponse
    {
        [JsonProperty(PropertyName = "result")]
        public string Result {get;set;}
    }
}