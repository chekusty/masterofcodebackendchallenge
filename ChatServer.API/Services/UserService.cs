using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChatServer.API.Data;
using ChatServer.API.Data.Models;

namespace ChatServer.API.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _usersRepository;
        public UserService(IUserRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public async Task<User> Authenticate(string username, string password)
        {
            var user = await _usersRepository.GetByName(username);

            if(user == null)
            {
                return null;
            }

            if(!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }

            return user;
        }

        public async Task<IList<User>> GetAll()
        {
            return await _usersRepository.GetAll();
        }

        public async Task<User> Register(User user, string password)
        {
            CreatePasswordHash(password, out byte[] hash, out byte[] salt);
            
            user.PasswordHash = hash;
            user.PasswordSalt = salt;

            return await _usersRepository.Create(user);
        }

        public async Task<User> GetById(Guid id)
        {
            return await _usersRepository.GetById(id);
        }

        public async Task<bool> UserExists(string username)
        {
            var user = await _usersRepository.GetByName(username);
            return user != null ? true : false;
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for(int i = 0; i < computedHash.Length; i++)
                {
                    if(computedHash[i] != passwordHash[i]) return false;
                }

                return true;
            }
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
    }
}