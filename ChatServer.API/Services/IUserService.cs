using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChatServer.API.Data.Models;

namespace ChatServer.API.Services
{
    public interface IUserService
    {
         Task<User> Authenticate(string username, string password);
         Task<IList<User>> GetAll();
         Task<User> GetById(Guid id);
         Task<bool> UserExists(string username);
         Task<User> Register(User user, string password);
    }
}