using System;
using System.Threading.Tasks;
using ChatServer.API.Data;
using ChatServer.API.Data.Models;
using ChatServer.API.Dtos;
using ChatServer.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ChatServer.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("rooms")]
    public class RoomsController : ControllerBase
    {
        private readonly IRoomsRepository _roomsRepository;
        private readonly IUserService _userService;

        public RoomsController(IRoomsRepository roomsRepository, IUserService userService)
        {
            _roomsRepository = roomsRepository;
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _roomsRepository.GetAll();
            return Ok(result);
        }

        [HttpGet("{roomId}")]
        public async Task<IActionResult> Get(string roomId)
        {
            if (!Guid.TryParse(roomId, out Guid guidId))
            {
                return BadRequest("Incorrect room id format.");
            }

            var roomWithUsers = await _roomsRepository.GetById(guidId);

            if (roomWithUsers == null)
            {
                return NotFound("Room with requested Id was not found.");
            }

            return Ok(roomWithUsers);
        }

        [HttpPost]
        public async Task<IActionResult> Create(RoomCreateDto roomToCreate)
        {
            if (!Guid.TryParse(roomToCreate.UserId, out Guid guidId))
            {
                return BadRequest("Incorrect room id format.");
            }

            var createdBy = await _userService.GetById(guidId);

            if (createdBy == null)
            {
                return BadRequest("User with provided id does not exist.");
            }

            var room = new Room
            {
                RoomName = roomToCreate.RoomName,
                CreatedBy = createdBy
            };

            var createdRoom = await _roomsRepository.Create(room);

            return StatusCode(201, room);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            if (!Guid.TryParse(id, out Guid guidId))
            {
                return BadRequest("Incorrect room id format.");
            }

            var roomWithUsers = await _roomsRepository.GetById(guidId);

            if (roomWithUsers == null)
            {
                return NotFound("Room with requested Id was not found.");
            }

            await _roomsRepository.Delete(roomWithUsers);
            var response = new MessageResponse
            {
                Result = "room removed successfully"
            };

            return Ok(response);
        }


        [HttpPost("{roomId}/join")]
        public async Task<IActionResult> Join(string roomId, [FromBody]UserForJoiningTheRoom userToJoin)
        {
            if (!Guid.TryParse(roomId, out Guid roomGuidId))
            {
                return BadRequest("Incorrect room id format.");
            }

            if (!Guid.TryParse(userToJoin.UserId, out Guid userGuidId))
            {
                return BadRequest("Incorrect user_id format.");
            }

            var user = await _userService.GetById(userGuidId);

            if (user == null)
            {
                return BadRequest("User with provided id does not exist.");
            }

            var room = await _roomsRepository.GetById(roomGuidId);

            if (room == null)
            {
                return BadRequest("The room with provided id does not exist.");
            }

            room.Users.Add(user);

            await _roomsRepository.Update(room);

            var result = new MessageResponse { Result = "user successfully joined the room" };

            return StatusCode(201, room);
        }
    }
}