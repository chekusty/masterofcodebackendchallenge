using ChatServer.API.Data.Models;
using ChatServer.API.Dtos;
using ChatServer.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(UserForLoginDto userParam)
        {
            if (await _userService.UserExists(userParam.Username))
            {   
                var user = await _userService.Authenticate(userParam.Username, userParam.Password);

                if (user == null)
                    return BadRequest(new { message = "Username or password is incorrect" });

                var result = TranslateModelToDto(user, userParam.Password);

                return Ok(result);
            }
            else
            {
                var userToCreate = new User { Username = userParam.Username };

                var user = await _userService.Register(userToCreate, userParam.Password);

                var result = TranslateModelToDto(user, userParam.Password);

                return StatusCode(201, result);
            }
        }

        private UserLoginResultDto TranslateModelToDto(User user, string password)
        {
            var result = new UserLoginResultDto();
            result.UserName = user.Username;
            result.UserId = user.Id;

            result.Credentials = GetCredentials(user.Username, password);

            return result;
        }

        private UserDto TranslateModelToDto(User user)
        {
            var result = new UserDto
            {
                UserName = user.Username,
                UserId = user.Id
            };
          
            return result;
        }

        private string GetCredentials(string userName, string password)
        {
            var credentialsRaw = $"{userName}:{password}";
            var credentialBytes = Encoding.UTF8.GetBytes(credentialsRaw);
            var credentials = Convert.ToBase64String(credentialBytes);

            return credentials;
        }

        [HttpGet]
        [Route("users")]
        public async Task<IActionResult> Get()
        {
            var users = await _userService.GetAll();
            var usersResult = new List<UserDto>(users.Count);
            foreach (var item in users)
            {
                var userResult = TranslateModelToDto(item);
                usersResult.Add(userResult);
            }
            return Ok(usersResult);
        }

        [HttpGet]
        [Route("users/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            if(!Guid.TryParse(id, out Guid guidId))
            {
                return BadRequest("Incorrect user id format.");
            }

            var user = await _userService.GetById(guidId);

            if(user == null)
            {
                return NotFound("User with requested Id was not found.");
            }

            var userResult = TranslateModelToDto(user);

            return Ok(userResult);
        }
    }
}