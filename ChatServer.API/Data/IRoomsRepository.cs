using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChatServer.API.Data.Models;

namespace ChatServer.API.Data
{
    public interface IRoomsRepository
    {
        Task<List<Room>> GetAll();
        Task<Room> GetById(Guid roomId);
        Task<Room> Create(Room room);
        Task Delete(Room room);
        Task Update(Room room);
    }
}