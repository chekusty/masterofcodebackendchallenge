using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatServer.API.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ChatServer.API.Data
{
    public class RoomsRepository : IRoomsRepository
    {
        private readonly DataContext _context;

        public RoomsRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<Room> Create(Room room)
        {
            await _context.Rooms.AddAsync(room);
            await _context.SaveChangesAsync();

            return room;
        }

        public async Task<List<Room>> GetAll()
        {
            return await _context.Rooms.Include(x=> x.Users).ToListAsync();
        }

        public async Task<Room> GetById(Guid roomId)
        {
            return await _context.Rooms.Include(x=> x.Users).FirstOrDefaultAsync();
        }

        public async Task Delete(Room room)
        {
            _context.Remove(room);
            await _context.SaveChangesAsync();
        }

        public async Task Update(Room room)
        {
            _context.Attach(room);
            await _context.SaveChangesAsync();
        }
    }
}