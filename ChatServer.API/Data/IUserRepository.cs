using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChatServer.API.Data.Models;

namespace ChatServer.API.Data
{
    public interface IUserRepository
    {
         Task<IList<User>> GetAll();
         Task<User> GetByName(string username);
         Task<User> GetById(Guid id);
         Task<User> Create(User userToCreate);
    }
}