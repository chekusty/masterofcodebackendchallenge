using ChatServer.API.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ChatServer.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }

        public DbSet<Room> Rooms { get; set; }
    }
}