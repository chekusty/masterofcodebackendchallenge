using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChatServer.API.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace ChatServer.API.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;

        public UserRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<User> Create(User userToCreate)
        {
            await _context.Users.AddAsync(userToCreate);
            await _context.SaveChangesAsync();
            
            return userToCreate;
        }

        public async Task<IList<User>> GetAll()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User> GetByName(string username)
        {
            return await _context.Users.FirstOrDefaultAsync(x=> x.Username == username);
        }

        
        public async Task<User> GetById(Guid id)
        {
            return await _context.Users.FirstOrDefaultAsync(x=> x.Id == id);
        }
    }
}