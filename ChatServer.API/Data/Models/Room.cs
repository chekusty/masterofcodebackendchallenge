using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ChatServer.API.Data.Models
{
    public class Room
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty(PropertyName = "room_id")]
        public Guid Id{get;set;}
        [JsonProperty(PropertyName = "room_name")]
        public string RoomName{get;set;}
        [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public ICollection<User> Users{get;set;}
        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt{get;set;}
        [JsonProperty(PropertyName = "creator_id")]
        public Guid CreatedById{get;set;}
        [JsonIgnore]
        public User CreatedBy{get;set;}
    }
}